package com.example.demohbase.config;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@org.springframework.context.annotation.Configuration
public class HBaseConfig {

	@Bean
	Configuration hBaseConfiguration() throws IOException, ServiceException {
		Configuration config = HBaseConfiguration.create();

		String path = this.getClass()
				.getClassLoader()
				.getResource("hbase-site.xml")
				.getPath();
		config.addResource(new Path(path));

		HBaseAdmin.checkHBaseAvailable(config);

		return config;
	}

}
