package com.example.demohbase;

import com.example.demohbase.extrato.Extrato;
import com.example.demohbase.extrato.ExtratoService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-04.
 */
@Component
public class ExtratoCLR implements CommandLineRunner {

	private static final Logger LOG = LoggerFactory.getLogger(ExtratoCLR.class);

	private final ExtratoService extratoService;

	public ExtratoCLR(ExtratoService extratoService) {
		this.extratoService = extratoService;
	}

	@Override
	public void run(String... args) throws Exception {

		if (extratoService.isTableExists()) return;

		long transactionId = 0L;

		Random r = new Random();
		for (int ag = 1; ag <= 100; ag++) {
			LOG.info("Gerando dados da agencia: {}", ag);
			List<Extrato> extratoList = new ArrayList<>();
			for (int cc = 1; cc <= 1000; cc++) {
				for (int dia=1; dia <= 90; dia++) {
					double v = (r.nextDouble()-0.8d)*100;
					Extrato extrato = Extrato
							.builder()
							.transactionId(++transactionId)
							.agencia(StringUtils.leftPad(String.valueOf(ag), 4, "0"))
							.conta(StringUtils.leftPad(String.valueOf(cc), 7, "0"))
							.data(data(dia))
							.descricao( (v > 0 ? "CREDITO" : "DEBITO") + " EM CONTA" )
							.valor(BigDecimal.valueOf(v))
							.build();
					extratoList.add(extrato);
				}
			}
			extratoService.salvar(extratoList);
		}
		LOG.info("Transacoes geradas: {}", transactionId);
	}

	private Date data(int ref) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -ref);
		return c.getTime();
	}

}
