package com.example.demohbase.utils;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-04.
 */
public interface HBaseHelper<T> {

	Table getTable(Connection connection) throws IOException;

	T value(Result result);

	byte[] getRowKey(T obj);

	Put put(T obj);

	boolean isTableExists(Connection connection) throws IOException;

}
