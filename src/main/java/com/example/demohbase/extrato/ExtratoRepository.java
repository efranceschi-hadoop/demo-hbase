package com.example.demohbase.extrato;

import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
public interface ExtratoRepository {

	List<Extrato> consultar(String agencia, String conta, Date dataInicio, Date dataTermino);

	void salvar(Extrato extrato);

	void salvar(List<Extrato> extratoList);

	boolean isTableExists();

}
