package com.example.demohbase.extrato;

import com.example.demohbase.utils.HBaseHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-04.
 */
@Component
public class ExtratoHelper implements HBaseHelper<Extrato> {

	private static final String TABLE_NAME = "extrato";

	private static final String CF_INFO = "i";

	private static final SimpleDateFormat sdfShort = new SimpleDateFormat("yyyyMMdd");

	@Override
	public Table getTable(Connection connection) throws IOException {
		TableName tableName = TableName.valueOf(TABLE_NAME);
		Admin admin = connection.getAdmin();
		if (!admin.tableExists(tableName)) {
			HTableDescriptor tableDescriptor = new HTableDescriptor(tableName);
			HColumnDescriptor cd = new HColumnDescriptor(CF_INFO);
			cd.setCompressionType(Compression.Algorithm.SNAPPY);
			tableDescriptor.addFamily(cd);
			admin.createTable(tableDescriptor);
		}
		return connection.getTable(tableName);
	}

	@Override
	public boolean isTableExists(Connection connection) throws IOException {
		TableName tableName = TableName.valueOf(TABLE_NAME);
		Admin admin = connection.getAdmin();
		return admin.tableExists(tableName);
	}

	@Override
	public Extrato value(Result result) {
		return Extrato
				.builder()
				.transactionId(Bytes.toLong(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("id"))))
				.data(new Date(Bytes.toLong(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("data")))))
				.descricao(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("desc"))))
				.agencia(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("ag"))))
				.conta(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("cc"))))
				.valor(Bytes.toBigDecimal(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("val"))))
				.build();
	}

	@Override
	public byte[] getRowKey(Extrato extrato) {
		return new StringBuilder()
				.append(StringUtils.reverse(extrato.getConta()))
				.append("-")
				.append(StringUtils.reverse(extrato.getAgencia()))
				.append("-")
				.append(sdfShort.format(extrato.getData()))
				.append("-")
				.append(extrato.getTransactionId())
				.toString()
				.getBytes();
	}

	@Override
	public Put put(Extrato extrato) {
		byte rowkey[] = getRowKey(extrato);
		Put put = new Put(rowkey);
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("id"), Bytes.toBytes(extrato.getTransactionId()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("data"), Bytes.toBytes(extrato.getData().getTime()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("desc"), Bytes.toBytes(extrato.getDescricao()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("ag"), Bytes.toBytes(extrato.getAgencia()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("cc"), Bytes.toBytes(extrato.getConta()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("val"), Bytes.toBytes(extrato.getValor()));
		return put;
	}

}
