package com.example.demohbase.extrato;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Controller
public class ExtratoController {

	private final ObjectMapper om = new ObjectMapper();
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private final ExtratoService extratoService;

	public ExtratoController(ExtratoService extratoService) {
		this.extratoService = extratoService;
	}

	@GetMapping(value = "/extrato/{agencia}/{conta}", produces = "text/json")
	@ResponseBody
	public String consulta(
			@PathVariable("agencia") String agencia,
			@PathVariable("conta") String conta,
			@RequestParam(value = "dataInicio", required = false, defaultValue = "") @DateTimeFormat(pattern="yyyy-MM-dd") Date dataInicio,
			@RequestParam(value = "dataTermino", required = false, defaultValue = "") @DateTimeFormat(pattern="yyyy-MM-dd") Date dataTermino
	) throws JsonProcessingException {

		if (dataInicio == null) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, -1);
			dataInicio = c.getTime();
		}

		if (dataTermino == null) {
			dataTermino = new Date();
		}

		List<Extrato> extrato = extratoService.consultar(agencia, conta, dataInicio, dataTermino);

		Map<String, Object> result = new HashMap<>();
		result.put("agencia", agencia);
		result.put("conta", conta);
		result.put("dataInicio", sdf.format(dataInicio));
		result.put("dataTermino", sdf.format(dataTermino));
		result.put("extrato", extrato);

		return om.writeValueAsString(result);
	}

}
