package com.example.demohbase.extrato;

import com.example.demohbase.utils.IteratorHelper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.*;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Repository
public class ExtratoRepositoryImpl implements ExtratoRepository {

	private final Configuration configuration;
	private final ExtratoHelper helper;

	public ExtratoRepositoryImpl(Configuration configuration, ExtratoHelper helper) {
		this.configuration = configuration;
		this.helper = helper;
	}

	@Override
	public List<Extrato> consultar(String agencia, String conta, Date dataInicio, Date dataTermino) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			byte startRow[] = helper.getRowKey(Extrato
					.builder()
					.agencia(agencia)
					.conta(conta)
					.data(dataInicio)
					.build()
			);
			byte stopRow[] = helper.getRowKey(Extrato
					.builder()
					.agencia(agencia)
					.conta(conta)
					.data(dataTermino)
					.build()
			);
			Scan scan = new Scan(startRow, stopRow);
			ResultScanner resultScanner = table.getScanner(scan);
			return new IteratorHelper(resultScanner.iterator(), helper).toList();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void salvar(Extrato extrato) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			Put put = helper.put(extrato);
			table.put(put);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void salvar(List<Extrato> extratoList) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			List<Put> puts = new ArrayList<>();
			extratoList.forEach(e -> puts.add(helper.put(e)));
			table.put(puts);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isTableExists() {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			return helper.isTableExists(connection);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
