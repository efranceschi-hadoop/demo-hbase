package com.example.demohbase.extrato;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Extrato {

	private Long transactionId;

	private String agencia;

	private String conta;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date data;

	private String descricao;

	private BigDecimal valor;

}
