package com.example.demohbase.extrato;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Service
public class ExtratoService {

	private final ExtratoRepository extratoRepository;

	public ExtratoService(ExtratoRepository extratoRepository) {
		this.extratoRepository = extratoRepository;
	}

	public List<Extrato> consultar(String agencia, String conta, Date dataInicio, Date dataTermino) {
		return extratoRepository.consultar(agencia, conta, dataInicio, dataTermino);
	}

	public void salvar(Extrato extrato) {
		extratoRepository.salvar(extrato);
	}

	public void salvar(List<Extrato> extratoList) {
		extratoRepository.salvar(extratoList);
	}

	public boolean isTableExists() {
		return extratoRepository.isTableExists();
	}
}
