# demo-hbase

Projeto HBase que simula leitura e gravação de dados de conta corrente

## Configurações do Ambiente
Antes de executar a aplicação, suba o container Docker:

```bash
docker run --name cloudera \
  --hostname=quickstart.cloudera \
  --privileged=true \
  -td \
  -v /mnt/user/cloudera-quickstart:/src \
  -p 2181:2181 \
  -p 8888:8888 \
  -p 7180:7180 \
  -p 6680:80 \
  -p 7187:7187 \
  -p 8079:8079 \
  -p 8080:8080 \
  -p 8085:8085 \
  -p 8400:8400 \
  -p 8161:8161 \
  -p 9090:9090 \
  -p 9095:9095 \
  -p 60000:60000 \
  -p 60010:60010 \
  -p 60020:60020 \
  -p 60030:60030 \
  cloudera/quickstart /usr/bin/docker-quickstart
```

Inclua também no seu arquivo `/etc/hosts` local o ip da máquina que está executando o Docker:

```
172.30.30.254  quickstart.cloudera
```

E por último, não esqueça de editar o arquivo de configuração do hbase `src/main/resources/hbase-site.xml`:

```xml
<configuration>
    <property>
        <name>hbase.zookeeper.quorum</name>
        <value>172.30.30.254</value>
    </property>
    <property>
        <name>hbase.zookeeper.property.clientPort</name>
        <value>2181</value>
    </property>
</configuration>
```

> Importante: As configurações acima consideram que a aplicação está rodando localmente e a máquina executando os containers Docker é `172.30.30.254`.
